# Cloud Integration - microservice - RestApi js

```docker
docker compose up
```

## file docker-compose.yml

```docker
version: "3.9"
services:
  microservice_lab1:
    image: lab1_microservice:0.0.1-SNAPSHOT
    ports:
      - "8080:8080"
    restart: on-failure
  database:
    image: postgres
    env_file:
      - .env
    volumes: 
        - ./postgres-data:/var/lib/postgresql/data
        - ./create_tables.sql:/docker-entrypoint-initdb.d/create_tables.sql
    ports:
      - "5432:5432"
  microservice_lab2:
    image: microservice_lab2
    ports:
      - "8081:8081"
```