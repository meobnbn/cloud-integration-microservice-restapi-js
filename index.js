const express = require('express')
const app = express()
const actors = require('./actors.json')

app.get('/actors', (req,res) => {
    res.status(200).json(actors)
})
app.get('/actors/:id', (req, res) => {
    const id = parseInt(req.params.id)
    const actor = actors.find(actor => actor.id === id)
    // setTimeout(function(){ 
    //     res.status(200).json(actor.actors)
    // }, 2000);  
    res.status(200).json(actor.actors)
})

app.listen(8081, () => {  
    console.log("Serveur à l'écoute")       
})